--[==[
    Quest Sync

    All Rights Reserved 
    
    © stpain 2020
]==]--


local QuestSyncName, QuestSync = ...

local L = {}
L['TooFarAway'] = 'too far away to receive' --locale version of the chat message '[Player] is too far away to receive quest'
L['NotEligible'] = 'is not eligible for that quest' --locale version of the chat message '[Player] is not eligible for quest'
L['PrintHelp'] = 'slash commands for quest sync\n  open - open the quest sync window\n  hide - close the quest sync window\n  debug - toggle debug settings\n  reset-cache - wipes the temp quest log cache (this holds data about quests shared via the addon and can be cleared anytime)'
L['Author'] = 'Author|cffffffff '
L['Version'] = 'Version|cffffffff '
L['QuestSync'] = 'Quest Sync '
L['FooterText'] = '|cff167e28GREEN|r=Quest Completed, |cff0687F5BLUE|r=Quest Accepted, |cff8D2D19RED|r=Quest NOT Completed or Accepted'
L['LeftClick'] = 'Left Click'
L['RightClick'] = 'Right Click'
L['OpenUI'] = 'Open UI'
L['Configure'] = 'Configure'
L['NotInParty'] = 'You are not in a Party, unable to retrieve quests.'
L['QuestTitleUnavailable'] = 'Quest Title Unavailable'
L['Page'] = 'Page '
L['ID'] = 'ID'
L['Debug'] = 'DEBUG'
L['DebugCB_Tooltip'] = 'DEBUG\n|cffffffffToggle debug messages in the chat window'
L['Zone'] = 'Zone'
L['Player'] = 'Player'
L['Title'] = 'Title'
L['Completed'] = 'Completed'
L['EmptyQuestLog'] = 'quest log is empty'
L['UnableToPushQuest'] = 'unable to push quest'
L['QuestPushed'] = 'pushed quest '
L['UnableToPushQuest'] = 'unable to push this quest'
L['ConfigText'] = 'Quest Sync was developed to help friends, guilds and groups share quests easier.\n\nWhen the addon window is opened it\'ll scan the quest logs of party members, these quests will then be displayed with a colour coded system.\n\n|cff167e28GREEN|r=Quest Completed, |cff0687F5BLUE|r=Quest Accepted, |cff8D2D19RED|r=Quest NOT Completed or Accepted.'
L['AvailableQuests'] = 'Available Quests'
L['ScanningForQuests'] = 'Scanning for quests...'
L['QuestListDescription'] = 'Quests available from PARTY members, normal rules apply to quest sharing. You must be eligible for the quest and be within sharing distance of the player with the quest. Quests will only sync if both players have the addon installed.'
L['OpenWithQuestLogCB_Text'] = "Open with Quest Log"
L['OpenWithQuestLogCB_Tooltip'] = 'Open with Quest Log\n|cffffffffOpens the Quest Sync UI whenever you open your Quest Log|r'
L['DisplayMinimapButtonCB_Text'] = "Display Minimap Button"
L['DisplayMinimapButtonCB_Tooltip'] = 'Display Minimap Button\n|cffffffffToggle the Minimap Button|r'

local locale = GetLocale()
--GERMAN
if locale == "deDE" then
    L['PrintHelp'] = 'slash commands for quest sync\n  open - open the quest sync window\n  hide - close the quest sync window\n  debug - toggle debug settings'
    L['Author'] = 'Author|cffffffff ' --keep the colour coding as is
    L['Version'] = 'Version|cffffffff ' --keep the colour coding as is
    L['QuestSync'] = 'Quest Sync '
    L['FooterText'] = '|cff167e28GREEN|r=Quest Completed, |cff0687F5BLUE|r=Quest Accepted, |cff8D2D19RED|r=Quest NOT Completed or Accepted' --keep the colour coding as is
    L['LeftClick'] = 'Left Click'
    L['RightClick'] = 'Right Click'
    L['OpenUI'] = 'Open UI'
    L['Configure'] = 'Configure'
    L['NotInParty'] = 'You are not in a Party, unable to retrieve quests.'
    L['QuestTitleUnavailable'] = 'Quest Title Unavailable'
    L['Page'] = 'Page '
    L['ID'] = 'ID'
    L['Debug'] = 'DEBUG'
    L['DebugCB_Tooltip'] = 'DEBUG\n|cffffffffToggle debug messages in the chat window' --keep the colour coding as is
    L['Zone'] = 'Zone'
    L['Player'] = 'Player'
    L['Title'] = 'Title'
    L['Completed'] = 'Completed'
    L['EmptyQuestLog'] = 'quest log is empty'
    L['UnableToPushQuest'] = 'unable to push quest'
    L['QuestPushed'] = 'pushed quest '
    L['UnableToPushQuest'] = 'unable to push this quest'
    L['ConfigText'] = 'Quest Sync was developed to help friends, guilds and groups share quests easier.\n\nWhen the addon window is opened it\'ll scan the quest logs of party members, these quests will then be displayed with a colour coded system.\n\n|cff167e28GREEN|r=Quest Completed, |cff0687F5BLUE|r=Quest Accepted, |cff8D2D19RED|r=Quest NOT Completed or Accepted.' --keep the colour coding as is
    L['AvailableQuests'] = 'Available Quests'
    L['ScanningForQuests'] = 'Scanning for quests...'
    L['QuestListDescription'] = 'Quests available from PARTY members, normal rules apply to quest sharing. You must be eligible for the quest and be within sharing distance of the player with the quest. Quests will only sync if both players have the addon installed.'
    L['OpenWithQuestLogCB_Text'] = "Open with Quest Log"
    L['OpenWithQuestLogCB_Tooltip'] = 'Open with Quest Log\n|cffffffffOpens the Quest Sync UI whenever you open your Quest Log|r' --keep the colour coding as is
    L['DisplayMinimapButtonCB_Text'] = "Display Minimap Button"
    L['DisplayMinimapButtonCB_Tooltip'] = 'Display Minimap Button\n|cffffffffToggle the Minimap Button|r' --keep the colour coding as is

--[==[ NOT YET IMPLMENMTED - REQUIRE TRANSLATORS

-- FRENCH
elseif locale == 'frFR' then
    L['PrintHelp'] = ''
    L['Author'] = ''
    L['Version'] = ''
    L['QuestSync'] = ''
    L['FooterText'] = ''
    L['LeftClick'] = ''
    L['RightClick'] = ''
    L['OpenUI'] = ''
    L['Configure'] = ''
    L['NotInParty'] = ''
    L['QuestTitleUnavailable'] = ''
    L['Page'] = ''
    L['ID'] = ''
    L['Debug'] = ''
    L['DebugCB_Tooltip'] = ''
    L['Zone'] = ''
    L['Player'] = ''
    L['Title'] = ''
    L['Completed'] = ''
    L['EmptyQuestLog'] = ''
    L['UnableToPushQuest'] = ''
    L['QuestPushed'] = ''
    L['UnableToPushQuest'] = ''
    L['ConfigText'] = ''
    L['AvailableQuests'] = ''
    L['ScanningForQuests'] = ''
    L['QuestListDescription'] = ''
    L['OpenWithQuestLogCB_Text'] = ''
    L['OpenWithQuestLogCB_Tooltip'] = ''
    L['DisplayMinimapButtonCB_Text'] = ''
    L['DisplayMinimapButtonCB_Tooltip'] = ''

-- ITALIAN
elseif locale == 'itIT' then
    L['PrintHelp'] = ''
    L['Author'] = ''
    L['Version'] = ''
    L['QuestSync'] = ''
    L['FooterText'] = ''
    L['LeftClick'] = ''
    L['RightClick'] = ''
    L['OpenUI'] = ''
    L['Configure'] = ''
    L['NotInParty'] = ''
    L['QuestTitleUnavailable'] = ''
    L['Page'] = ''
    L['ID'] = ''
    L['Debug'] = ''
    L['DebugCB_Tooltip'] = ''
    L['Zone'] = ''
    L['Player'] = ''
    L['Title'] = ''
    L['Completed'] = ''
    L['EmptyQuestLog'] = ''
    L['UnableToPushQuest'] = ''
    L['QuestPushed'] = ''
    L['UnableToPushQuest'] = ''
    L['ConfigText'] = ''
    L['AvailableQuests'] = ''
    L['ScanningForQuests'] = ''
    L['QuestListDescription'] = ''
    L['OpenWithQuestLogCB_Text'] = ''
    L['OpenWithQuestLogCB_Tooltip'] = ''
    L['DisplayMinimapButtonCB_Text'] = ''
    L['DisplayMinimapButtonCB_Tooltip'] = ''

-- RUSSIAN
elseif locale == 'ruRU' then
    L['PrintHelp'] = ''
    L['Author'] = ''
    L['Version'] = ''
    L['QuestSync'] = ''
    L['FooterText'] = ''
    L['LeftClick'] = ''
    L['RightClick'] = ''
    L['OpenUI'] = ''
    L['Configure'] = ''
    L['NotInParty'] = ''
    L['QuestTitleUnavailable'] = ''
    L['Page'] = ''
    L['ID'] = ''
    L['Debug'] = ''
    L['DebugCB_Tooltip'] = ''
    L['Zone'] = ''
    L['Player'] = ''
    L['Title'] = ''
    L['Completed'] = ''
    L['EmptyQuestLog'] = ''
    L['UnableToPushQuest'] = ''
    L['QuestPushed'] = ''
    L['UnableToPushQuest'] = ''
    L['ConfigText'] = ''
    L['AvailableQuests'] = ''
    L['ScanningForQuests'] = ''
    L['QuestListDescription'] = ''
    L['OpenWithQuestLogCB_Text'] = ''
    L['OpenWithQuestLogCB_Tooltip'] = ''
    L['DisplayMinimapButtonCB_Text'] = ''
    L['DisplayMinimapButtonCB_Tooltip'] = ''

-- SPANISH
elseif locale == "esES" then
    L['PrintHelp'] = ''
    L['Author'] = ''
    L['Version'] = ''
    L['QuestSync'] = ''
    L['FooterText'] = ''
    L['LeftClick'] = ''
    L['RightClick'] = ''
    L['OpenUI'] = ''
    L['Configure'] = ''
    L['NotInParty'] = ''
    L['QuestTitleUnavailable'] = ''
    L['Page'] = ''
    L['ID'] = ''
    L['Debug'] = ''
    L['DebugCB_Tooltip'] = ''
    L['Zone'] = ''
    L['Player'] = ''
    L['Title'] = ''
    L['Completed'] = ''
    L['EmptyQuestLog'] = ''
    L['UnableToPushQuest'] = ''
    L['QuestPushed'] = ''
    L['UnableToPushQuest'] = ''
    L['ConfigText'] = ''
    L['AvailableQuests'] = ''
    L['ScanningForQuests'] = ''
    L['QuestListDescription'] = ''
    L['OpenWithQuestLogCB_Text'] = ''
    L['OpenWithQuestLogCB_Tooltip'] = ''
    L['DisplayMinimapButtonCB_Text'] = ''
    L['DisplayMinimapButtonCB_Tooltip'] = ''

    ]==]--

end

QuestSync.L = L