# QuestSync

QuestSync is a World of Warcraft addon to help players share quests when they join a group.

The addon will only share quests with other players using the addon.