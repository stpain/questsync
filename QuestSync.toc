## Interface: 11300
## Author: S T Pain (|r|cffF58CBAAethreos|r |cffffffff/|r |cffABD473Thornbow|r|cffffffff - Pyrewood Village)
## Version: 1.0.2
## Title: |cffABD473QuestSync|r
## SavedVariables: QuestSyncGlobalSettings

Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.lua
Libs\LibDataBroker-1.1\LibDataBroker-1.1.lua
Libs\LibDbIcon-1.0\LibDbIcon-1.0.lua

QuestSync_Locales.lua
QuestSync_Util.lua
QuestSync_Core.lua

QuestSync_Options.xml
QuestSync.xml