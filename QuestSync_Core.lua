--[==[
    Quest Sync

    All Rights Reserved 
    
    © stpain 2020
]==]--

local QuestSyncName, QuestSync = ...

QuestSync.Debug = false --default to off

local L = QuestSync.L --bring locales into local scope

--local keywords, saves on global lookups
local tinsert = table.insert
local tconcat = table.concat
local type, pairs, ipairs = type, pairs, ipairs
local tostring, tonumber = tostring, tonumber

--addon vars
local FetchRequestData = {} --experimental
local name, realm = UnitName('player'), GetRealmName() -- will these go straight into tostring, do they always and only return this info? - tostring(UnitName('player')..'-'..GetRealmName()):gsub("%s+", "")
local PLAYER_REALM = tostring(name..'-'..realm):gsub("%s+", "") --remove space in realm names if exists
local CHAT_CHANNEL = 'PARTY' -- 'WHIPSER' --whisper doesnt work as intended
local ScanEndTime = nil --used for the progress bar
local QuestSyncUI = {} --manages the main UI of the addon
local QuestSyncIO = {} --manages commands and data between addons
local QuestSyncCache = {} --temp quest log/id cache
local UI = { --UI config
    ScanningTime = 1, --progress bar time, essentially a faux delay to help with any chat channel throttle
    ScanningSBColour = QuestSync.Util.RgbToPercent({22,126,40}), --progress bar colour
    CurrentPage = 1,
    Rows = {}, --table to contain the UI rows
    RowConfig = { --row font string config
        Font = "Fonts\\FRIZQT__.TTF",
        FontSize = 12,
        FontInherit = 'GameFontNormal',
        RGB = {r=1, g=1, b=1},
        SyncButtonInherit = 'UIPanelButtonTemplate'
    },
    FontColours = { --returns a 0-1 based number for the rgb %
        QuestCompleted = QuestSync.Util.RgbToPercent({22,126,40}), --green=completed quest
        QuestExists = QuestSync.Util.RgbToPercent({6,135,245}), --blue=quest exists in log
        QuestRequired = QuestSync.Util.RgbToPercent({141,45,25}), --red=quest required
    }
}

------------------------------------------------------------------------------------------------------------------
--slash commands
SLASH_QuestSync1 = '/questsync'
SlashCmdList.QuestSync = function(msg)
	msg = msg:lower()
	if msg == 'help' then
        QuestSync.Util.Print(L['PrintHelp'])  
	elseif msg == 'open' then
		QuestSyncParent:Show()
	elseif msg == 'hide' then
        QuestSyncParent:Hide()
    elseif msg == 'debug' then
        QuestSyncGlobalSettings[UnitGUID('player')].Debug = not QuestSyncGlobalSettings[UnitGUID('player')].Debug
        QuestSync.Debug = QuestSyncGlobalSettings[UnitGUID('player')].Debug
        QuestSyncOptionsInterfaceDebugCB:SetChecked(QuestSyncGlobalSettings[UnitGUID('player')].Debug)
    elseif msg == 'reset-cache' then
        QuestSyncCache = {}
        QuestSync.Util.Print('quest log cache has been wiped')
	end
end

------------------------------------------------------------------------------------------------------------------
--main menu options interface
function QuestSyncOptionsInterface_OnLoad(self)
    self.name = QuestSyncName
    InterfaceOptions_AddCategory(self)
end

------------------------------------------------------------------------------------------------------------------
function QuestSyncOptionsInterfaceDebugCB_OnLoad(self)
    _G[self:GetName()..'Text']:SetText(L['Debug'])
    self.tooltip = L['DebugCB_Tooltip']
end

function QuestSyncOptionsInterfaceDebugCB_OnClick(self)
    QuestSyncGlobalSettings[UnitGUID('player')].Debug = not QuestSyncGlobalSettings[UnitGUID('player')].Debug
    QuestSync.Debug = QuestSyncGlobalSettings[UnitGUID('player')].Debug
end

function QuestSyncOptionsInterfaceOpenWithQuestLogCB_OnLoad(self)
    _G[self:GetName()..'Text']:SetText(L['OpenWithQuestLogCB_Text'])
    self.tooltip = L['OpenWithQuestLogCB_Tooltip']
end

function QuestSyncOptionsInterfaceOpenWithQuestLogCB_OnClick(self)
    QuestSyncGlobalSettings[UnitGUID('player')].OpenWithQuestLog = not QuestSyncGlobalSettings[UnitGUID('player')].OpenWithQuestLog
    if QuestSyncGlobalSettings[UnitGUID('player')].OpenWithQuestLog == true then
        QuestSyncParent:SetParent(QuestLogFrame)
        QuestSyncParent:Show()
    else
        QuestSyncParent:SetParent(UIParent)
        QuestSyncParent:Hide()
    end
end

function QuestSyncOptionsInterfaceDisplayMinimapButtonCB_OnLoad(self)
    _G[self:GetName()..'Text']:SetText(L['DisplayMinimapButtonCB_Text'])
    self.tooltip = L['DisplayMinimapButtonCB_Tooltip']
end

function QuestSyncOptionsInterfaceDisplayMinimapButtonCB_OnClick(self)
    QuestSyncGlobalSettings[UnitGUID('player')].DisplayMinimapButton = not QuestSyncGlobalSettings[UnitGUID('player')].DisplayMinimapButton
    if QuestSyncGlobalSettings[UnitGUID('player')].DisplayMinimapButton == false then
        QuestSync.MinimapIcon:Hide('QuestSyncMinimapIcon')
    else
        QuestSync.MinimapIcon:Show('QuestSyncMinimapIcon')
    end
end

function QuestSyncParentPageUp_OnClick()
    QuestSyncUI:ClearRows()
    if UI.CurrentPage == 8 then --is 8 a good upper limit, 5 players in group -> 4 others sending full log of 20 quests = 80 quests divide 10 (ui rows)
        QuestSyncUI:RefreshRows(UI.CurrentPage)
        return
    else
        UI.CurrentPage = UI.CurrentPage + 1
        QuestSyncUI:RefreshRows(UI.CurrentPage)
    end
end

function QuestSyncParentPageDown_OnClick()
    QuestSyncUI:ClearRows()
    if UI.CurrentPage == 1 then --limit negative pages
        QuestSyncUI:ClearRows()
        QuestSyncUI:RefreshRows(UI.CurrentPage)
        return
    else
        UI.CurrentPage = UI.CurrentPage - 1
        QuestSyncUI:ClearRows()
        QuestSyncUI:RefreshRows(UI.CurrentPage)
    end
end

-- update UI and send fetch request TODO: add a refresh button to send the fetch command while UI is open
function QuestSyncParent_OnShow(self)
    FetchRequestData = {}
    QuestSyncParent_ProgressText:Show()
    if IsInGroup() == false then
        QuestSyncParent_ProgressText:SetText(L['NotInParty'])
        QuestSyncParent_PageNumberText:SetText(L['Page']..1)
    else
        QuestSyncUI:ClearRows()
        QuestSyncParent_PageNumberText:SetText(L['Page']..UI.CurrentPage)
        QuestSyncParent_ProgressText:Show()
        QuestSyncParent_ProgressText:SetText(L['ScanningForQuests'])
        ScanEndTime = (GetTime() + UI.ScanningTime)
        QuestSyncParent_ScanningSB:Show()
        QuestSyncParent_ScanningSB:SetValue(0)
        local success = C_ChatInfo.SendAddonMessage("QS_FetchQuests", 'fetch-quests', 'SAY') --using for testing move into PARTY or RAID (RAID falls back to PARTY if not in raid)
        if success == true then
            local ticker = C_Timer.After(tonumber(UI.ScanningTime), function() --delay this to allow addon chat messages send/receive, blizzard may throttle channels not sure of consequence to this addon
                QuestSyncParent_ProgressText:Hide()
                QuestSyncParent_ScanningSB:Hide()
                QuestSyncParent_DescriptionText:SetText(L['QuestListDescription'])
                QuestSyncUI:RefreshRows(1) --(UI.CurrentPage)
            end)
        else
            QuestSync.Util.Debug('failed to send fetch request')
        end
    end
end

function QuestSyncParent_OnHide(self)
    ScanEndTime = nil -- reset the progress bar
end

--load settings and options
function QuestSync.QuestSync_Init()
    --create saved settings
    if QuestSyncGlobalSettings == nil then
        QuestSyncGlobalSettings = {}
    end
    if QuestSyncGlobalSettings[UnitGUID('player')] == nil then
        QuestSyncGlobalSettings[UnitGUID('player')] = {
            OpenWithQuestLog = false,
            DisplayMinimapButton = true,
            OnlyShowUncollectedQuests = false,
            Debug = false,
            MinimapButton = nil,
        }
    end

    --update check boxes
    QuestSyncOptionsInterfaceOpenWithQuestLogCB:SetChecked(QuestSyncGlobalSettings[UnitGUID('player')].OpenWithQuestLog)
    QuestSyncOptionsInterfaceDisplayMinimapButtonCB:SetChecked(QuestSyncGlobalSettings[UnitGUID('player')].DisplayMinimapButton)
    QuestSyncOptionsInterfaceDebugCB:SetChecked(QuestSyncGlobalSettings[UnitGUID('player')].Debug)
    QuestSync.Debug = QuestSyncGlobalSettings[UnitGUID('player')].Debug

    --create minimap button
    QuestSyncUI:CreateMinimapIcon()
    if QuestSyncGlobalSettings[UnitGUID('player')].DisplayMinimapButton == false then
        QuestSync.MinimapIcon:Hide('QuestSyncMinimapIcon')
    else
        QuestSync.MinimapIcon:Show('QuestSyncMinimapIcon')
    end

    QuestSyncIO:RegisterChatPrefixes()
    QuestSyncUI:DrawRows()
    QuestSyncUI:Init() --only the UI init stuff
    
end

--update the progress bar
function QuestSyncParent_OnUpdate()
    if ScanEndTime then
        if tonumber(GetTime()) < tonumber(ScanEndTime) then
            QuestSyncParent_ScanningSB:SetValue((tonumber(UI.ScanningTime) - ( tonumber(ScanEndTime) - tonumber(GetTime()) )) * (100.0 / tonumber(UI.ScanningTime)))
        end
    end
end

--handle events
function QuestSyncParent_OnEvent(self, event, ...)
    if event == "ADDON_LOADED" and tostring(select(1, ...)):lower() == "questsync" then
        local version = GetAddOnMetadata(QuestSyncName, "Version")
        QuestSync.Util.Print(tostring('loaded (version '..version..')'))
        QuestSync.Util.MakeFrameMove(self)
        local init = C_Timer.After(1, function() QuestSync.QuestSync_Init() end) --delay to allow GUID to be available - is this still the case?

    elseif event == 'QUEST_ACCEPTED' then --used to update the ui, will change the quest colour
        local questLogIndex = select(1, ...)
        local questID = select(2, ...)
        for k, quest in ipairs(QuestSyncCache) do
            if tonumber(quest.QuestID) == tonumber(questID) then
                quest.Exists = true
            end
        end
        QuestSyncUI:ClearRows()
        QuestSyncUI:RefreshRows(UI.CurrentPage)

    elseif event == 'CHAT_MSG_SYSTEM' then --send info regarding quest share fails
        local msg = select(1, ...)        
        if string.find(msg:lower(), L['TooFarAway']) then --should use the locale version of the chat message '[Player] is too far away to receive quest'
            local infoMsg = C_ChatInfo.SendAddonMessage('QS_InfoMsg', tostring(msg), CHAT_CHANNEL)
            if infoMsg == false then
                QuestSync.Util.Debug('failed sending info message - '..msg)
            end
        elseif string.find(msg, L['NotEligible']) then --should use the locale version of the chat message '[Player] is not eligible for quest'
            local infoMsg = C_ChatInfo.SendAddonMessage('QS_InfoMsg', tostring(msg), CHAT_CHANNEL)
            if infoMsg == false then
                QuestSync.Util.Debug('failed sending info message - '..msg)
            end
        end

    elseif event == 'CHAT_MSG_ADDON' then --pass any addon prefix messages to the handler
        local prefix = select(1, ...)
        local msg = select(2, ...)
        local sender = select(4, ...)
        if (prefix == 'QS_FetchQuests') or (prefix == 'QS_ParseData') or (prefix == 'QS_PushQuest') or (prefix == 'QS_Error') or (prefix == 'QS_InfoMsg') then
            QuestSyncIO[prefix](_, msg, sender)
        end
    end
end

------------------------------------------------------------------------------------------------------------------
-- manage the UI
QuestSyncUI = {
    CreateMinimapIcon = function()
        local ldb = LibStub("LibDataBroker-1.1")
        QuestSync.MinimapButton = ldb:NewDataObject('QuestSyncMinimapIcon', {
            type = "data source",
            icon = 132115,
            OnClick = function(self, button)
                if button == "RightButton" then
                    InterfaceOptionsFrame_OpenToCategory(QuestSyncName)
                    InterfaceOptionsFrame_OpenToCategory(QuestSyncName)
                elseif button == 'LeftButton' then
                    if QuestSyncParent:IsVisible() then
                        QuestSyncParent:Hide()
                    else
                        QuestSyncParent:Show()
                    end
                end
            end,
            OnTooltipShow = function(tooltip)
                if not tooltip or not tooltip.AddLine then return end
                tooltip:AddLine(tostring('|cffABD473'..L['QuestSync']))
                tooltip:AddLine(tostring('|cffFFFFFF'..L['LeftClick']..': |r'..L['OpenUI']))
                tooltip:AddLine(tostring('|cffFFFFFF'..L['RightClick']..': |r'..L['Configure']))
            end,
        })
        QuestSync.MinimapIcon = LibStub("LibDBIcon-1.0")
        if not QuestSyncGlobalSettings[UnitGUID('player')].MinimapButton then QuestSyncGlobalSettings[UnitGUID('player')].MinimapButton = {} end
        QuestSync.MinimapIcon:Register('QuestSyncMinimapIcon', QuestSync.MinimapButton, QuestSyncGlobalSettings[UnitGUID('player')].MinimapButton)
    end,
    Init = function(self)
        QuestSyncParent_TitleText:SetText(L['QuestSync'])    
        QuestSyncParent_DescriptionText:SetText(L['QuestListDescription'])
        QuestSyncParent_IDHeader:SetText(L['ID'])
        QuestSyncParent_ZoneHeader:SetText(L['Zone'])
        QuestSyncParent_TitleHeader:SetText(L['Title'])
        QuestSyncParent_PlayerHeader:SetText(L['Player'])
        QuestSyncParent_FooterText:SetText(L['FooterText'])       
        QuestSyncOptionsInterface_TitleText:SetText(L['QuestSync'])        
        local version = GetAddOnMetadata(QuestSyncName, "Version")
        QuestSyncOptionsInterface_VersionText:SetText(L['Version']..version)
        local author = GetAddOnMetadata(QuestSyncName, "Author")
        QuestSyncOptionsInterface_AuthorText:SetText(L['Author']..author)
        QuestSyncOptionsInterface_DescriptionText:SetText(L['ConfigText'])
        QuestSyncParent_ScanningSB:Hide()
        QuestSyncParent_ScanningSB:SetStatusBarColor(unpack(UI.ScanningSBColour))        
    end,
    DrawRows = function(self)
        local verticalOffset, rowHeight = 95, 22
        for i = 1, 10 do
            tinsert(UI.Rows, {
                Sender = nil,
                QuestID = nil,
                IDText = QuestSync.Util.NewFontString(QuestSyncParent, tostring('QuestSyncIDRow'..i), 'TOPLEFT', 20, (((i * -1) * rowHeight) - verticalOffset), ' ', UI.RowConfig.RGB, UI.RowConfig.Font, UI.RowConfig.FontSize, UI.RowConfig.FontInherit),
                ZoneText = QuestSync.Util.NewFontString(QuestSyncParent, tostring('QuestSyncZoneRow'..i), 'TOPLEFT', 80, (((i * -1) * rowHeight) - verticalOffset), ' ', UI.RowConfig.RGB, UI.RowConfig.Font, UI.RowConfig.FontSize, UI.RowConfig.FontInherit),
                TitleText = QuestSync.Util.NewFontString(QuestSyncParent, tostring('QuestSyncTitleRow'..i), 'TOPLEFT', 220, (((i * -1) * rowHeight) - verticalOffset), ' ', UI.RowConfig.RGB, UI.RowConfig.Font, UI.RowConfig.FontSize, UI.RowConfig.FontInherit),
                PlayerText = QuestSync.Util.NewFontString(QuestSyncParent, tostring('QuestSyncCharacterRow'..i), 'TOPLEFT', 500, (((i * -1) * rowHeight) - verticalOffset), ' ', UI.RowConfig.RGB, UI.RowConfig.Font, UI.RowConfig.FontSize, UI.RowConfig.FontInherit),
                SyncButton = QuestSync.Util.NewButton(QuestSyncParent, tostring('QuestSyncButton'..i), 'TOPLEFT', 600, (((i * -1) * rowHeight) - verticalOffset + 2), 85, 20, 'Sync Quest', UI.RowConfig.SyncButtonInherit, false),
            })
        end
    end,
    ClearRows = function(self)
        for k, row in ipairs(UI.Rows) do
            row.Sender = nil
            row.QuestID = nil
            row.IDText:SetText(' ')
            row.ZoneText:SetText(' ')
            row.TitleText:SetText(' ')
            row.PlayerText:SetText(' ')
            row.SyncButton:Hide()
        end
    end,
    RefreshRows = function(self, page)
        local i, lower, upper = 1, ((page * 10) - 9), ((page * 10) + 1)
        for k, quest in ipairs(QuestSyncCache) do
            if k >= lower and k < upper then
                local player = string.sub(quest.Sender, 1, string.find(quest.Sender, '-') - 1)
                local title = C_QuestLog.GetQuestInfo(quest.QuestID) or L['QuestTitleUnavailable']
                if UI.Rows[i] then
                    UI.Rows[i].Sender = quest.Sender
                    UI.Rows[i].QuestID = quest.QuestID
                    UI.Rows[i].IDText:SetText(tostring('['..quest.QuestID..']'))
                    UI.Rows[i].ZoneText:SetText(quest.Zone)
                    UI.Rows[i].TitleText:SetText(title)
                    UI.Rows[i].PlayerText:SetText(player) --(quest.Sender)
                    UI.Rows[i].SyncButton:Show()
                    UI.Rows[i].SyncButton:SetScript('OnClick', function() QuestSyncIO:SendPullQuestRequest(quest.QuestID, quest.Sender) end) --AcceptQuest() ? auto accept quest when pops up, needs testing
                    --update font colours
                    if quest.Completed == true then
                        UI.Rows[i].TitleText:SetTextColor(unpack(UI.FontColours.QuestCompleted)) 
                    elseif quest.Completed == false then
                        UI.Rows[i].TitleText:SetTextColor(unpack(UI.FontColours.QuestRequired))
                    end
                    --apply after
                    if quest.Exists == true then
                        UI.Rows[i].TitleText:SetTextColor(unpack(UI.FontColours.QuestExists))
                    end
                end
            i = i + 1
            end
        end
        QuestSyncParent_PageNumberText:SetText(L['Page']..page)
    end,
}

------------------------------------------------------------------------------------------------------------------
-- manage addon chat messages and events
QuestSyncIO = {
    RegisterChatPrefixes = function(self)
        local regChatFetch = C_ChatInfo.RegisterAddonMessagePrefix("QS_FetchQuests") --used to send requests
        local regChatData = C_ChatInfo.RegisterAddonMessagePrefix("QS_ParseData") --used to send data
        local regChatPush = C_ChatInfo.RegisterAddonMessagePrefix("QS_PushQuest") --used to send data
        local regChatError = C_ChatInfo.RegisterAddonMessagePrefix("QS_Error") --used to send data
        local regChatInfoMsg = C_ChatInfo.RegisterAddonMessagePrefix("QS_InfoMsg") --used to send data
        if regChatFetch == false then
            QuestSync.Util.Debug('error registering chat prefix - fetch')
        elseif regChatData == false then
            QuestSync.Util.Debug('error registering chat prefix - data')
        elseif regChatPush == false then
            QuestSync.Util.Debug('error registering chat prefix - push')
        elseif regChatError == false then
            QuestSync.Util.Debug('error registering chat prefix - error')
        elseif regChatInfoMsg == false then
            QuestSync.Util.Debug('error registering chat prefix - info msg')
        end
    end,
    SendPullQuestRequest = function(self, questID, sender) --send a request to party members, if party member has quest then they attempt to push quest
        if sender ~= PLAYER_REALM then
            QuestSync.Util.Debug('sending pull quest request to - '..sender)
            local questPull = C_ChatInfo.SendAddonMessage('QS_PushQuest', questID, CHAT_CHANNEL, sender)
            if questPull == false then
                QuestSync.Util.Debug('error sending quest pull, quest id - ', questID)
            end
        end
    end,

    -- the remaining functions use a prefix as the func name
    QS_InfoMsg = function(self, msg, sender)
        if sender ~= PLAYER_REALM then
            QuestSync.Util.Print(tostring(sender..' - '..msg))
        end
    end,
    QS_Error = function(self, msg, sender)
        if sender ~= PLAYER_REALM then
            QuestSync.Util.Print(tostring('error from '..sender..' '..msg))
        end
    end,
    QS_ParseData = function(self, dataSent, sender) --parse quest id's from party members
        QuestSync.Util.Debug(tostring('Function - QS_ParseQuest, dataSent '..dataSent..', sender '..sender))

        --experimental stuff - ignore
        if not FetchRequestData[sender] then FetchRequestData[sender] = {} end
        tinsert(FetchRequestData[sender], dataSent)

        if sender ~= PLAYER_REALM then
            QuestSync.Util.Debug('parsing data from '..sender)
            if not QuestSyncCache then
                QuestSyncCache = {}
            end
            local i, questIDs = 1, {}
            for d in string.gmatch(dataSent, '[^:]+') do --split the quest IDs string
                questIDs[i] = d
                i = i + 1
            end
            local myQuestLog = {}
            local numEntries, numQuests = GetNumQuestLogEntries()        
            for i = 1, numEntries do
                local title, level, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID, startEvent, displayQuestID, isOnMap, hasLocalPOI, isTask, isBounty, isStory, isHidden, isScaling = GetQuestLogTitle(i)	
                if isHeader == false then
                    tinsert(myQuestLog, questID) --get client quest log data
                end
            end
            local questsCompleted, count = GetQuestsCompleted(), 1
            for k = 2, #questIDs do -- questID[1]=zone or quest heading
                local questDone, questInLog, cached = false, false, false
                for id, complete in pairs(questsCompleted) do --loop completed quests
                    if tonumber(id) == tonumber(questIDs[k]) and complete == true then
                        questDone = true
                    end
                end
                for i, quest in pairs(myQuestLog) do --loop my quest log
                    if tonumber(quest) == tonumber(questIDs[k]) then
                        questInLog = true
                    end
                end
                for _, quest in ipairs(QuestSyncCache) do --loop cache and ignore duplicates
                    if quest.QuestID == questIDs[k] then
                        cached = true
                    end
                end
                if cached == false then --ignores duplicate quests
                    tinsert(QuestSyncCache, { Zone = questIDs[1], Sender = sender, QuestID = questIDs[k], Completed = questDone, Exists = questInLog } )
                    count = count + 1 --was used for something somewhen?
                end
            end
        end
    end,
    QS_FetchQuests = function(self, msg, sender) --gets quest id's from client
        QuestSync.Util.Debug(tostring('Function - QS_FetchQuest, msg '..msg..', sender '..sender))
        if sender ~= PLAYER_REALM then
            QuestSync.Util.Debug(tostring('fetch quests cmd from '..sender..' '..msg))
            local questLogIDs, zone, c = {}, 'Unknown', 0
            local numEntries, numQuests = GetNumQuestLogEntries()
            if tonumber(numQuests) < 1 then
                local errorSent = C_ChatInfo.SendAddonMessage("QS_Error", L['EmptyQuestLog'], CHAT_CHANNEL, sender)
                return
            else
                for i = 1, numEntries do
                    local title, level, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID, startEvent, displayQuestID, isOnMap, hasLocalPOI, isTask, isBounty, isStory, isHidden, isScaling = GetQuestLogTitle(i)	
                    if isHeader == true then
                        zone = title
                        c = c + 1
                    end
                    if isHeader == false then
                        if not questLogIDs[zone] then questLogIDs[zone] = {} end
                        tinsert(questLogIDs[zone], questID)
                    end
                end
                for zone, quests in pairs(questLogIDs) do
                    local questsSent = C_ChatInfo.SendAddonMessage("QS_ParseData", tostring(zone..':'..tconcat(quests, ':')), CHAT_CHANNEL, sender) --using WHISPER to target and reduce message bulk
                    if questsSent == false then
                        QuestSync.Util.Debug('error sending quest log data for '..zone)
                    end
                end
            end
        end
    end,
    QS_PushQuest = function(self, request, sender) --check quest log for matching quest and if found pushes it
        --print(sender)
        QuestSync.Util.Debug(tostring('Function - QS_PushQuest, request '..request..', sender '..sender))
        if sender ~= PLAYER_REALM then
            QuestSync.Util.Debug('push request not from me, checking quest log to share quest')
            local questLogIDs = {}
            local numEntries, numQuests = GetNumQuestLogEntries()        
            for i = 1, numEntries do
                local title, level, suggestedGroup, isHeader, isCollapsed, isComplete, frequency, questID, startEvent, displayQuestID, isOnMap, hasLocalPOI, isTask, isBounty, isStory, isHidden, isScaling = GetQuestLogTitle(i)	
                if isHeader == false then
                    if tonumber(questID) == tonumber(request) then
                        local logIndex = GetQuestLogIndexByID(request)
                        SelectQuestLogEntry(logIndex)
                        if GetQuestLogPushable() == true then
                            local title = C_QuestLog.GetQuestInfo(request)
                            local infoMsgSent = C_ChatInfo.SendAddonMessage("QS_InfoMsg", tostring(L['QuestPushed']..' - '..title), CHAT_CHANNEL, sender) -- CHAT_CHANNEL)
                            QuestLogPushQuest()
                        else
                            local title = C_QuestLog.GetQuestInfo(request)
                            local errorSent = C_ChatInfo.SendAddonMessage("QS_Error", tostring(L['UnableToPushQuest']..' - '..title), CHAT_CHANNEL, sender)
                        end
                    end
                end
            end
        else
            QuestSync.Util.Debug('push request from me, ignoring request')
        end
    end,
}