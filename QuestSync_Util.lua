--[==[
    Quest Sync

    All Rights Reserved 
    
    © stpain 2020
]==]--

local QuestSyncName, QuestSync = ...

QuestSync.Util = {
    Print = function(msg)
        print(tostring('|cffABD473QuestSync |r'..msg))
    end,
    Debug = function(msg)
        if QuestSync.Debug == true then
            print(tostring('|cffC41F3BQuestSync DEBUG: |r'..msg))
        end
    end,
    MakeFrameMove = function(frame)
        frame:SetMovable(true)
        frame:EnableMouse(true)
        frame:RegisterForDrag("LeftButton")
        frame:SetScript("OnDragStart", frame.StartMoving)
        frame:SetScript("OnDragStop", frame.StopMovingOrSizing)
    end,
    GetArgs = function(...)
        for i=1, select("#", ...) do
            arg = select(i, ...)
            print(i.." "..tostring(arg))
        end
    end,
    RgbToPercent = function(t)
        if type(t) == 'table' then
            if type(t[1]) == 'number' and type(t[2]) == 'number' and type(t[3]) == 'number' then
                local r = tonumber(t[1] / 256.0)
                local g = tonumber(t[2] / 256.0)
                local b = tonumber(t[3] / 256.0)
                return {r, g, b}
            end
        end
    end,
    NewButton = function(parent, name, anchor, x, y, w, h, text, inherits, visible)
        if (parent == nil) or (name == nil) or (inherits == nil) then QuestSync.Util.Debug('creating button - parent, name or inherit missing') return end
        if (type(x) ~= 'number') or (type(y) ~= 'number') or(type(w) ~= 'number') or (type(h) ~= 'number') then QuestSync.Util.Debug('creating button - x,y,w or h not numbers') return end
        local b = CreateFrame('BUTTON', name, parent, inherits) -- 'UIPanelButtonTemplate')
        b:SetPoint(anchor, x, y)
        b:SetSize(w, h)
        b:SetText(text)
        if visible == true then
            b:Show()
        elseif visible == false then
            b:Hide()
        end
        return b
    end,
    NewFontString = function(parent, name, anchor, x, y, text, rgb, font, fontSize, inherits)
        if (parent == nil) or (name == nil) or (inherits == nil) then QuestSync.Util.Debug('creating fontstring - parent, name or inherit missing') return end
        if (type(x) ~= 'number') or (type(y) ~= 'number') then QuestSync.Util.Debug('creating fontstring - x or y not numbers') return end
        local fs = parent:CreateFontString(name, 'OVERLAY', inherits)
        fs:SetPoint(anchor, x, y)
        fs:SetText(text)
        if rgb ~= nil then
            fs:SetTextColor(rgb.r, rgb.g, rgb.b)
        end
        if (font ~= nil) and (fontSize ~= nil) then
            fs:SetFont(font, fontSize)
        end
        return fs
    end,
}